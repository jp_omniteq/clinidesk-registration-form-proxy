const express = require("express");
const helmet = require("helmet");
const { createProxyMiddleware } = require("http-proxy-middleware");
const axios = require("axios");
var fs = require('fs')
var https = require('https')
var http = require('http')
var forceSSL = require('express-force-ssl');
// const morgan = require("morgan");
const path = require('path');
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}
  
// process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const app = express();

if (!process.env.HELMET_OFF) {
  app.use(helmet());
}
// app.use(morgan("dev"));
app.use(express.static(path.resolve(__dirname, './client/build')));

const PASSPHRASE = process.env.PASSPHRASE;
const PORT = process.env.PORT;
const PORT_SSL = process.env.PORT_SSL;
const HOST = process.env.HOST;
const FM_HOST = process.env.FM_HOST;;
const DBNAME = process.env.DBNAME;
const API_SESSION_URL =
  FM_HOST + "/fmi/data/v2/databases/" + DBNAME + "/sessions";

const username = process.env.USERNAME;;
const pass = process.env.PASS;
const usetNamePass = username + ":" + pass;
const buff = Buffer.from(usetNamePass, "utf-8");
const base64 = buff.toString("base64");

// set up a route to redirect http to https
app.use(forceSSL);

// const onProxyReq = async (proxyReq, req, res) => {
   
//   const response = await axios.post(API_SESSION_URL, "{}", {
//     headers: {
//       "Content-Type": "application/json",
//       Authorization: "Basic " + base64,
//     },
//   });
//   const token = response.data.response.token;

//   proxyReq.setHeader("Authorization", "Bearer " + token);
// };

// app.get('*', (req, res) => {
//   res.sendFile(path.resolve(__dirname, './client/build', 'index.html'));
// });

app.use((req, res, next) => {
  axios
    .post(API_SESSION_URL, "{}", {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Basic " + base64,
      },
    })
    .then((response) => {
      const token = response.data.response.token;
      req.headers["Authorization"] = "Bearer " + token;

      next();
    });
});

// Info GET endpoint
// app.get("/info", (req, res, next) => {
//   res.send("Hello world!");
// });

// Proxy endpoints
app.use(
  "/api/prospect",
  createProxyMiddleware({
    //ssl: false,
    logLevel: "debug",
    secure: false,
    target: FM_HOST,
    changeOrigin: true,
    pathRewrite: {
      [`^/api/prospect`]: "/fmi/data/v2/databases/" + DBNAME + "/layouts/_Person_api/script/createProspectFromForm",
    },
  })
);

app.use(
  "/api/file",
  createProxyMiddleware({
    logLevel: "debug",
    secure: false,
    target: FM_HOST,
    changeOrigin: true,
    pathRewrite: {
      [`^/api/file`]: "/fmi/data/v2/databases/" + DBNAME + "/layouts/_File_api/records",
    },
  })
);



app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});


// Start the Proxy
// app.listen(PORT, HOST, () => {
//   console.log(`Starting Proxy at ${HOST}:${PORT}`);
// });
// app.listen(PORT_SSL, HOST, () => {
//   console.log(`Starting Proxy at ${HOST}:${PORT_SSL}`);
// });


http.createServer(app).listen(PORT)
https.createServer({
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('crt.pem'),
  passphrase: PASSPHRASE
}, app)
.listen(PORT_SSL, function () {
  console.log(`Starting Proxy at ${HOST}:${PORT_SSL}`)
})